# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name        = 'euclidean_sequencer'
  spec.version     = '0.0.2'
  spec.date        = '2018-05-04'
  spec.summary     = 'Generates sequences of hits spaced as evenly as possible \
    across given pulses.'
  spec.description = 'Generates sequences of hits spaced as evenly as possible \
    across given pulses using the Euclidean algorithm.'
  spec.authors     = ['Matt Ridenour']
  spec.email       = 'mattridenour@gmail.com'
  spec.homepage    = 'https://gitlab.com/Mattbot/gem-euclidean-sequencer'
  spec.license       = 'MIT'
  spec.files         = `git ls-files -z`.split('\x0').reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 10.0'
end
