# frozen_string_literal: true

require './lib/euclidean_sequencer/parser.rb'

RSpec.describe Parser do
  let(:sequence) { '10101' }

  describe '#parse' do
    it 'excepts the hits arguement' do
      argv = ['-h23']
      result = Parser.parse(argv)
      expect(result[:hits]).to eq(23)

      argv = ['--hits=123']
      result = Parser.parse(argv)
      expect(result[:hits]).to eq(123)
    end

    it 'excepts the pulses arguement' do
      argv = ['-p32']
      result = Parser.parse(argv)
      expect(result[:pulses]).to eq(32)

      argv = ['--pulses=132']
      result = Parser.parse(argv)
      expect(result[:pulses]).to eq(132)
    end

    it 'excepts hits arguement' do
      argv = ['-ayes']
      result = Parser.parse(argv)
      expect(result[:array]).to eq(true)
    end

    it 'excepts offset arguement' do
      argv = ['-o423']
      result = Parser.parse(argv)
      expect(result[:offset]).to eq(423)

      argv = ['--offset=1423']
      result = Parser.parse(argv)
      expect(result[:offset]).to eq(1423)
    end
  end
end
