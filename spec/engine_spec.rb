# frozen_string_literal: true

require './lib/euclidean_sequencer/engine.rb'

RSpec.describe Engine do
  describe '#run' do
    it 'Runs the Euclidean algorithm with 0/0' do
      expect(Engine.run(hits: 0, pulses: 0)).to eq('')
    end

    it 'Runs the Euclidean algorithm with 0/1' do
      expect(Engine.run(hits: 0, pulses: 1)).to eq('0')
    end

    it 'Runs the Euclidean algorithm with 1/3' do
      expect(Engine.run(hits: 1, pulses: 3)).to eq('100')
    end

    it 'Runs the Euclidean algorithm with 4/8' do
      expect(Engine.run(hits: 4, pulses: 8)).to eq('10101010')
    end
  end
end
