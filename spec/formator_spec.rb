# frozen_string_literal: true

require './lib/euclidean_sequencer/formator.rb'

RSpec.describe Formator do
  let(:sequence) { '10101' }

  describe '#format_sequence' do
    it 'formats the sequence as a string' do
      result = Formator.format_sequence(
        sequence: sequence,
        options: { array: nil, offset: nil }
      )
      expect(result).to eq('10101')
    end

    it 'formats the sequence as an array' do
      result = Formator.format_sequence(
        sequence: sequence,
        options: { array: true, offset: nil }
      )
      expect(result).to eq([1, 0, 1, 0, 1])
    end

    it 'formats the sequence as an offset string' do
      result = Formator.format_sequence(
        sequence: sequence,
        options: { array: nil, offset: 1 }
      )
      expect(result).to eq('01011')
    end

    it 'formats the sequence as an offset array' do
      result = Formator.format_sequence(
        sequence: sequence,
        options: { array: true, offset: 1 }
      )
      expect(result).to eq([0, 1, 0, 1, 1])
    end
  end
end
