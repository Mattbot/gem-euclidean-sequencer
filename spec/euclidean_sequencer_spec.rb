# frozen_string_literal: true

require './lib/euclidean_sequencer.rb'

RSpec.describe EuclideanSequencer do
  describe '#generate' do
    it 'Generates a Euclidean sequence with 0/0' do
      expect(EuclideanSequencer.generate(hits: 0, pulses: 0)).to eq('')
    end

    it 'Generates a Euclidean sequence with 0/1' do
      expect(EuclideanSequencer.generate(hits: 0, pulses: 1)).to eq('0')
    end

    it 'Generates a Euclidean sequence with 1/1' do
      expect(EuclideanSequencer.generate(hits: 1, pulses: 1)).to eq('1')
    end

    it 'Generates a Euclidean sequence with 4/8' do
      expect(EuclideanSequencer.generate(hits: 4, pulses: 8)).to eq('10101010')
    end

    it 'Generates a Euclidean sequence with 4/8 with offset' do
      result = EuclideanSequencer.generate(
        hits: 4,
        pulses: 8,
        options: { offset: 1 }
      )
      expect(result).to eq('01010101')
    end

    it 'Generates a Euclidean sequence with 4/8 as array' do
      result = EuclideanSequencer.generate(
        hits: 4,
        pulses: 8,
        options: { array: true }
      )
      expect(result).to eq([1, 0, 1, 0, 1, 0, 1, 0])
    end

    it 'Generates a Euclidean sequence with 4/8 as array with offset' do
      result = EuclideanSequencer.generate(
        hits: 4,
        pulses: 8,
        options: { array: true, offset: 1 }
      )
      expect(result).to eq([0, 1, 0, 1, 0, 1, 0, 1])
    end
  end
end
