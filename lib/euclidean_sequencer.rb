# frozen_string_literal: true

require './lib/euclidean_sequencer/sequencer.rb'

# Main Gem class for generating sequences.
module EuclideanSequencer
  def self.generate(hits: 0, pulses: 0, options: { array: nil, offset: nil })
    run(hits, pulses, options)
  end

  private_class_method def self.run(hits, pulses, options)
    Sequencer.generate(hits: hits, pulses: pulses, options: options)
  end
end
