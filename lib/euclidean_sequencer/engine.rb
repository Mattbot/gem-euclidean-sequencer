# frozen_string_literal: true

# The magickal euclidean algorithmical engine!
class Engine
  def self.run(hits:, pulses:)
    euclid(hits, pulses)
  end

  private_class_method def self.euclid(hits, pulses, sequence = nil)
    # Contruct the sequence if new, otherwise continue with current sequence:
    sequence ||= generate_sequence(hits, pulses)

    # Determine if the algorithm has concluded or
    # another iteration is neccessy:
    return sequence.sum('') if hits.zero?

    # Count the sequence element types (remainder or denominator) for
    # distribution:
    remainder, denominator = find_remainder_and_denominator(sequence)

    # Determine how many sets of zeros can be nabbed from the pulses at a time,
    # insuring all the zeros get used up before the algorithm ends.
    zero_sets = find_zero_sets(
      hits,
      pulses,
      remainder,
      denominator,
      sequence.length
    )

    # Distribute the remainder into the denominator and
    # pop the unnecessary elements:
    sequence = distribute_and_pop_sequence(
      sequence,
      zero_sets,
      remainder,
      denominator
    )

    # Recursion:
    euclid(pulses % hits, hits, sequence)
  end

  private_class_method def self.generate_sequence(hits, pulses)
    sequence = []
    pulses.times { |pulse| sequence[pulse] = '0' }
    hits.times { |hit| sequence[hit] = '1' }
    sequence
  end

  private_class_method def self.find_remainder_and_denominator(sequence)
    remainder = 0
    denominator = 0
    sequence.each do |element|
      element == sequence.last ? remainder += 1 : denominator += 1
    end
    [remainder, denominator]
  end

  private_class_method def self.find_zero_sets(
    hits,
    pulses,
    remainder,
    denominator,
    sequence_length
  )
    if pulses > hits * 2
      # Must avoid division by zero and
      # grab as many zeros as possible:
      unless hits.zero?
        zero_sets = denominator.zero? ? 1 : remainder / denominator
        # But don't grab as many zeros as impossible and
        # scrapping the bottom of the zero barrel:
        zero_sets = zero_sets > sequence_length ? 0 : 1
      end
    else
      # The ratio of hits to pulses isn't so low as to start hording zeros:
      zero_sets = 1
    end
    zero_sets
  end

  private_class_method def self.distribute_and_pop_sequence(
    sequence,
    zero_sets,
    remainder,
    denominator
  )
    # Choose the approiate counter for the current size of the sequence:
    distributions = remainder > denominator ? denominator : remainder

    unfrozen = deeper_dup(sequence.dup)
    distributions.times do |dist|
      zero_sets.times do
        unfrozen[dist] << unfrozen.pop unless unfrozen[dist].nil?
      end
    end
    unfrozen
  end

  private_class_method def self.deeper_dup(array)
    # Because frozen_string_literal: true
    array.each_with_index { |element, index| p array[index] = element.dup }
  end
end
