# frozen_string_literal: true

require 'optparse'

# Organizes CLI options for processing.
class Parser
  def self.parse(argv)
    options = Struct.new(:hits, :pulses, :array, :offset)
    args = options.new

    opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: euclidean_sequncer [options]'

      opts.on(
        '-h',
        '--hits=HITS',
        Integer,
        'Number of "on" elements in the sequncer: INTEGER'
      ) do |hits|
        args.hits = hits
      end

      opts.on(
        '-pPULSES',
        '--pulses=PULSES',
        Integer,
        'Total number of elements in the sequncer: INTEGER'
      ) do |pulses|
        raise StandardError if pulses.nil?
        args.pulses = pulses
      end

      opts.on(
        '-a',
        '--array=TRUE',
        FalseClass,
        'Output as array (optional): BOOLEAN'
      ) do |array|
        args.array = array
      end

      opts.on(
        '-o',
        '--offset=OFFSET',
        Integer,
        'Number elements left shifted in the sequence (optional): INTEGER'
      ) do |offset|
        args.offset = offset
      end

      opts.on('-?', '--help', 'Prints this help') do
        puts opts
        exit
      end
    end

    begin
      opt_parser.parse!(argv)
    rescue StandardError => error
      printf('ERROR: %s \n', error)
      opt_parser.parse!(['--help'])
      exit 1
    end

    args
  end
end
