# frozen_string_literal: true

# Formats sequences for output.
class Formator
  def self.format_sequence(sequence:, options: { array: nil, offset: nil })
    formated = options[:array] ? convert_to_array(sequence) : sequence
    options[:offset] ? offset(formated, options[:offset]) : formated
  end

  private_class_method def self.convert_to_array(sequence)
    array = []
    sequence.length.times { |element| array[element] = sequence[element].to_i }
    array
  end

  private_class_method def self.offset(sequence, offset)
    if sequence.is_a?(String)
      return offset_array(sequence.split(//), offset).join
    end
    offset_array(sequence, offset)
  end

  private_class_method def self.offset_array(array, offset)
    offset.times { array << array.shift }
    array
  end
end
