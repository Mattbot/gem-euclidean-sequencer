# frozen_string_literal: true

require './lib/euclidean_sequencer/engine'
require './lib/euclidean_sequencer/formator'

# Central class for generating and formatting sequences.
class Sequencer
  def self.generate(hits: 0, pulses: 0, options: { array: nil, offset: nil })
    format_sequence(run(hits, pulses), options)
  end

  private_class_method def self.run(hits, pulses)
    Engine.run(hits: hits, pulses: pulses)
  end

  private_class_method def self.format_sequence(sequence, options)
    Formator.format_sequence(sequence: sequence, options: options)
  end
end
