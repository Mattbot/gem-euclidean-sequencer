# Euclidean Sequencer

by Matt Ridenour (mattridenour@gmail.com)
Created on Sun Oct 26 2008 (Updated at Wed May 2 2018)

A class for spaceing a number of ones over a range of zeros as evenly as possible using the [Euclidean algorithm][euclidean_algorithm].

For more information on using the Euclidean algorithm in the [context of musicial rhythms][euclidean_rhythms], please read:
[The Euclidean Algorithm Generates Traditional Musical Rhythms by Godfried Toussaint][toussaint].

## Usage:

Build Gem:
```bash
cp .envrc.example .envrc
vi .envrc
brew install direnv # macOS
apt-get install direnv # Ubuntu
direnv allow
setup
build
gem install euclidean_sequencer-*.gem
```

Pry Console:
```bash
console
pry(main)> cd EuclideanSequencer
pry(EuclideanSequencer):1> ls
...
```

Command Line:
```bash
./bin/euclidean_sequence --help
```
Ruby:
```ruby
require "./lib/sequencer.rb"
pulses = 8
hits = 5
p Sequencer.generate(hits: hits ,pulses: pulses, options: {array: true, offset: 2})
```

Oontz!

## License
[MIT][mit]

## Code of Conduct
[Code of Conduct][code_of_conduct]

[code_of_conduct]: CODE_OF_CONDUCT.md
[euclidean_algorithm]: https://en.wikipedia.org/wiki/Euclidean_algorithm
[euclidean_rhythms]: https://en.wikipedia.org/wiki/Euclidean_rhythm
[mit]: LICENSE.txt
[toussaint]: http://cgm.cs.mcgill.ca/~godfried/publications/banff.pdf
[wesen]: http://ruinwesen.com/blog?id=216

